# Willkommen

Dies ist ein interaktives Skript für die 1. Hälfte der Physik II Vorlesung an der Universität Hamburg im Wintersemester 23/24.  
Das Skript basiert auf [Jupyter Book](https://jupyterbook.org/). 

:::{note}
Dieses Skript ersetzt weder den Mitschrieb der Vorlesung noch das Lesen eines Buches.
:::

## Quellen

Dieses Jupyter Book basiert auf bereits existierenden online Skripten, insbesondere auf [Dem Vorlesungsskript Messtechnik der HSU](https://kisleif.github.io/mtbook/). Der Inhalt basiert auf diversen Büchern zur Physik 2 und Vorlesungsskripten des Fachbereichs Physik der Universität Hamburg.

## Jupyter Book, Jupyter, Python, Markdown, LaTeX

Dieses Skript kann direkt verwendet werden. Noch nützlicher wird es, wenn Sie mit den Methoden vertraut sind mit denen es erstellt wurde. Insbesondere die Nutzung von Python und LaTeX wird für Ihr weiteres Studium sehr nützlich sein. 

Mehr Informationen dazu finden Sie auf der Seite [**Nutzung**](content/usage.ipynb).